Init()

brush.addEventListener('click', function () {
    Utilities.update('brush')
    console.log('brush_enabled')
}, false)

eraser.addEventListener('click', function () {
    Utilities.update('eraser')
    console.log('eraser_enabled')
}, false)

text.addEventListener('click', function () {
    Utilities.update('text')
    console.log('text_enabled')
}, false)

reset.addEventListener('click', function () {
    Reset()
    Utilities.default()
    History.save()
}, false)

undo.addEventListener('click', function () {
    History.undo()
}, false)

redo.addEventListener('click', function () {
    History.redo()
}, false)

upload.addEventListener('click', function () {
    var reader = document.createElement("input")
    reader.type = "file"
    reader.onchange = (function () {
        //img = null;
        img = new Image();
        img.onload = function () {
            Utilities.reset()
            Utilities.update('img')
            console.log('img loaded!')
            History.save()
        }
        img.onerror = function () {
            console.log('error loading img!')
        }
        img.src = window.URL.createObjectURL(this.files[0])
    })
    reader.click()
}, false)

download.addEventListener('click', function () {
    var DLlink = document.createElement("a")
    DLlink.download = "img.png"
    DLlink.href = c.toDataURL()
    DLlink.click()
}, false)

configure1.addEventListener('change', function () {
    if (Utilities.text_enabled) {
        global_width = configure1.value;
    } 
    else {
        global_width = configure1.value;
    }
}, false)

configure2.addEventListener('change', function () {
    if (Utilities.text_enabled) {
        global_font = configure2.value;
    }
}, false)

color.addEventListener('change', function () {
    global_color = color.value;
}, false)

circle.addEventListener('click', function () {
    Utilities.update('circle')
}, false)

rectangle.addEventListener('click', function () {
    Utilities.update('rectangle')
}, false)

triangle.addEventListener('click', function () {
    Utilities.update('triangle')
}, false)

fill.addEventListener('click', function () {
    Fill()
    History.save()
}, false)

con.addEventListener('click', function (event) {
    if (Utilities.text_enabled && !mouse_locked) {
        mouse_locked = true;
        var input_box = document.createElement("input")
        input_box.type = "text"
        input_box.style.position = "absolute"
        input_box.style.left = event.offsetX.toString() + "px";
        input_box.style.top = event.offsetY.toString() + "px";
        input_box.style.zIndex = 999;
        con.appendChild(input_box);
        input_box.addEventListener('keyup', function (ev) {
            if(ev.keyCode === 13) {
                Draw_Fixed (input_box.style.left, input_box.style.top, input_box.value)
                con.removeChild(input_box)
                mouse_locked = false;
                History.save()
            }
            else if(ev.keyCode === 27) {
                con.removeChild(input_box)
                mouse_locked = false;
            }
        })
    }
    else if (Utilities.img_enabled && !mouse_locked) {
        mouse_locked = true;
        var cx = event.offsetX;
        var cy = event.offsetY;
        var scale_box = document.createElement("input")
        scale_box.type = "range"
        scale_box.className = "slider"
        scale_box.width = "100px";
        scale_box.style.position = "absolute"
        scale_box.style.left = cx + "px";
        scale_box.style.top = (cy - 15) + "px";
        scale_box.min = 1;
        scale_box.max = 100;
        scale_box.value = 100;
        scale_box.style.zIndex = 999;
        con.appendChild(scale_box);
        Draw_Fixed (cx, cy, scale_box.value)
        scale_box.addEventListener('mousedown', function (ev) {
            if (mouse_locked) {
                scale_box.addEventListener('input', function () {
                    History.load(function () {
                        Draw_Dynamic (cx, cy, scale_box.value)
                    })
                })
            }
        })
        scale_box.addEventListener('mouseup', function (ev) {
            if (mouse_locked) {
                History.load(function () {
                    Draw_Fixed (cx, cy, scale_box.value)
                    History.save()
                    Utilities.default()
                })
                con.removeChild(scale_box)
                mouse_locked = false;
            }
        })
    }
})

con.addEventListener('mousedown', function (event) {
    mouse_pressed = true;
    lastX = event.offsetX;
    lastY = event.offsetY;
    Draw (lastX, lastY)
}, false)

con.addEventListener('mousemove', function (event) {
    //console.log(`${event.offsetX}, ${event.offsetY}`)
    //Cursor_Color_Pick()
    globalX = event.offsetX;
    globalY = event.offsetY;
    if (Utilities.img_enabled && !mouse_locked) {
        History.load(function () {
            Draw_Dynamic (event.offsetX, event.offsetY)
        })
    }
    else if (Utilities.circle_enabled && mouse_pressed) {
        History.load(function () {
            Draw_Dynamic (event.offsetX, event.offsetY)
        })
    }
    else if (Utilities.rectangle_enabled && mouse_pressed) {
        History.load(function () {
            Draw_Dynamic (event.offsetX, event.offsetY)
        })
    }
    else if (Utilities.triangle_enabled && mouse_pressed) {
        History.load(function () {
            Draw_Dynamic (event.offsetX, event.offsetY)
        })
    }
    else if (mouse_pressed) {
        Draw (event.offsetX, event.offsetY)
    }
}, false)

con.addEventListener('mouseup', function (event) {
    mouse_pressed = false;
    if (Utilities.brush_enabled || Utilities.eraser_enabled) {
        History.save()
    }
    else if (Utilities.circle_enabled) {
        Draw_Fixed (event.offsetX, event.offsetY)
        History.save()
    }
    else if (Utilities.rectangle_enabled) {
        Draw_Fixed (event.offsetX, event.offsetY)
        History.save()
    }
    else if (Utilities.triangle_enabled) {
        Draw_Fixed (event.offsetX, event.offsetY)
        History.save()
    } 
    
}, false)