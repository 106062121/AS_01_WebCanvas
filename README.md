# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

| **Item**                                         | **Score** |
| :----------------------------------------------: | :-------: |
| Basic components                                 | 60%       |
| Advance tools                                    | 35%       |
| Appearance (subjective)                          | 5%        |
| Other useful widgets (**describe on README.md**) | 1~10%     |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

---

## Put your report below here


<img src="example02.gif" width="700px" height="500px">

* **brush**
    Enabled by default, press the bursh button above to trigger it manually. Simply drag and draw to use the tool! (If you accidentally moved outside the canvas, don't panic, just click on the canvas again!)
* **eraser**
    Press the eraser button above to trigger, drag and draw and erase what you draw! _(reminder: eraser clears to transparent!)_
* **text**
    Press the text button above to trigger, click on the place you want to start placing your text, a text box would appear. Click on the text box to start typing, press ENTER when you are done. Press ESC to cancel your input. (You have to click on the text box first)
* **reset**
    Press the reset button above to trigger, clears the current layer to transparent.
* **undo**
    Press the undo button above to trigger, return to last step.
* **redo**
    Press the redo button above to trigger, goes to the last step returned.
* **upload**
    Press the upload button above to trigger, an upload window will appear, select your image and upload it. Move your mouse to select where you want to place your image and click to determine. A slider will appear onclick. Move the slider to choose how much you want to scale your image.
* **download**
    Press the download button above to trigger, downloads current layer. If you have multiple layers, be sure to merge them before downloading!
* **circle**
    Press the circle button above to trigger, press to select the center of the circle and drag to select the radius. The tool will draw a filled circle.
* **rectangle**
    Press the rectangle button above to trigger, press to select the left-up corner of the rectangle and drag to select the right-down corner.
* **triangle**
    Press the triangle button above to trigger, press and drag to create a right triangle. the right angle will be on the y-axis.
* **fill**
    Press the fill button above to trigger, fills the whole layer to a color.
* **select-font**
    Selects the font for text input. Click to select.
* **select-width**
    Selects the width for all tools and inputs. Click to select.
* **select-color**
    The box-like element beside the two select boxes. Selects the color for all tools and inputs. Click to select.
* **layer-tools**
    * The plus button adds a new layer of canvas. _(alert: You can't redo this step!)_
    * The minus button beside deletes all layers except layer_0. _(alert: You can't redo this step!)_
    * The M button merges all layers to layer_0. _(alert: You can't redo this step!)_
* **layer**
    The right sidebar indicates current layer status. Layers with greater index has greater priorty. A different color of block shows which layer you are currently on. The preview window above usually shows the status of the layer you are on. There are several functions:
    * **preview**
        This button shows the layer you want to preview on the preview window.
    * **select**
        This button allows you to choose layers you want to work on.
    * **hide / show**
        This button hides or shows the chosen layer.
    * **delete**
        The minus button deletes the layer. Note that you **can't** delete the layer you are currently working on. _(alert: You can't redo this step!)_