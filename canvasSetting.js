var con = document.getElementById("canvas-body")
var stat = document.getElementById("right-panel")
var preview = document.getElementById("preview")
var c = document.getElementById("myCanvas")
var ctx = c.getContext("2d")

c.width = con.offsetWidth;
c.height = con.offsetHeight;
c.style.position = "absolute";
c.style.left = "0px";
c.style.top = "0px";
c.style.zIndex = "100"

var Layer = {
    layers: [],
    current: Number(c.style.zIndex) - 100,

    New_Layer: function () {
        c = document.createElement("canvas")
        ctx = c.getContext("2d")
        c.id = "myCanvas" + this.layers.length.toString()
        c.width = con.offsetWidth;
        c.height = con.offsetHeight;
        c.style.position = "absolute";
        c.style.left = "0px";
        c.style.top = "0px";
        c.style.zIndex = (100 + this.layers.length).toString()
        con.appendChild(c)
        var blk = document.createElement("div")
        blk.id = "layer-" + (this.layers.length).toString()
        blk.className = "layer";
        if (this.layers.length < 10)
            blk.innerHTML = "layer_" + (this.layers.length).toString() + " " + "&nbsp&nbsp"
        else 
            blk.innerHTML = "layer_" + (this.layers.length).toString() + " "
        blk.style.width = "100%";
        blk.style.paddingLeft = "10px";
        //blk.style.paddingTop = "3px";
        //blk.style.paddingBottom = "3px";
        var sel = document.createElement("button")
        sel.innerHTML = "select";
        sel.className = "sel";
        sel.id = "sel" + (this.layers.length).toString()
        var hid = document.createElement("button")
        hid.innerHTML = "hide";
        hid.className = "hid";
        hid.id = "hid" + (this.layers.length).toString()
        var del = document.createElement("button")
        del.innerHTML = "-";
        del.className = "del";
        del.id = "del" + (this.layers.length).toString()
        var pre = document.createElement("button")
        pre.innerHTML = "preview";
        pre.className = "pre";
        pre.id = "pre" + (this.layers.length).toString()
        blk.appendChild(pre)
        blk.appendChild(sel)
        blk.appendChild(hid)
        blk.appendChild(del)
        stat.appendChild(blk)

        handler(this.layers.length)

        this.layers.push(c)
        this.Update()
        History.add()
    },

    Delete_Layer: function () {
        c = this.layers[0];
        ctx = c.getContext("2d")
        for (let i = this.layers.length-1; i > 0; i--) {
            con.removeChild(this.layers[i])
            stat.removeChild(document.getElementById('layer-' + i.toString()))
            this.layers.pop()
        }
        this.Update()
        History.clear()
        History.save()
    },

    Merge_Layer: function () {
        c = this.layers[0];
        ctx = c.getContext("2d")
        for (let i = this.layers.length-1; i > 0; i--) {
            if (document.getElementById('layer-' + i.toString()).style.display != "none")
                ctx.drawImage(this.layers[i], 0, 0)
            con.removeChild(this.layers[i])
            stat.removeChild(document.getElementById('layer-' + i.toString()))
            this.layers.pop()
            console.log(`current at ${i}`);
        }
        this.Update()
        History.clear()
        History.save()
    },

    Update: function () {
        if (document.getElementById('layer-' + this.current.toString()))
            document.getElementById('layer-' + this.current.toString()).style.backgroundColor = "rgb(83, 83, 83)"
        this.current = Number(c.style.zIndex) - 100;
        document.getElementById('layer-' + this.current.toString()).style.backgroundColor = "rgb(128, 128, 128)"
        //Listener_Regen()
    },

    Select: function (index) {
        console.log(`current at ${this.current}, select ${index}`);
        if (index != this.current && index < this.layers.length) {
            c = this.layers[index];
            ctx = c.getContext("2d")
            this.Update()
        }
        this.Preview(index)
    },

    Hide: function (index) {
        console.log(`hide ${index}, current at ${this.current}`);
        if (index < this.layers.length) {
            if (this.layers[index].style.visibility === "hidden") {
                this.layers[index].style.visibility = "visible";
                document.getElementById('hid' + index.toString()).innerHTML = "hide";
            }
            else {
                this.layers[index].style.visibility = "hidden";
                document.getElementById('hid' + index.toString()).innerHTML = "show";
            }
        }
    },
    Delete: function (index) {
        if (index != this.current && index < this.layers.length && index != 0) {
            this.layers[index].style.display = "none"
            document.getElementById('layer-' + index.toString()).style.display = "none"
            //this.layers.splice(index, 1)
            //this.Update()
            //History.history.splice(index)
            //History.redo_list.splice(index)
            console.log(`current at ${this.current}`);
        }
    },
    Preview: function (index) {
        preview.src = History.history[index][History.history[index].length-1];
    }
}

Layer.layers.push(c)
Layer.Update()
//Listener_Regen()
var selP = document.getElementById('sel0')
var hidP = document.getElementById('hid0')
var delP = document.getElementById('del0')
var preP = document.getElementById('pre0')
selP.addEventListener('click', function () {
    Layer.Select(0)
}, false)
hidP.addEventListener('click', function () {
    Layer.Hide(0)
}, false)
delP.addEventListener('click', function () {
    Layer.Delete(0)
}, false)
preP.addEventListener('click', function () {
    Layer.Preview(0)
}, false)

var new_layer = document.getElementById('new')
new_layer.addEventListener('click', function () {
    Layer.New_Layer()
}, false)

var delete_layer = document.getElementById('del')
delete_layer.addEventListener('click', function () {
    if (Layer.current > 0) {
        Layer.Delete_Layer()
    }
}, false)

var merge_layer = document.getElementById('merge')
merge_layer.addEventListener('click', function () {
    Layer.Merge_Layer()
}, false)

function handler (index) {
    var sel = document.getElementById('sel' + index.toString())
    var hid = document.getElementById('hid' + index.toString())
    var del = document.getElementById('del' + index.toString())
    var pre = document.getElementById('pre' + index.toString())
    sel.addEventListener('click', function () {
        Layer.Select(index)
    }, false)
    hid.addEventListener('click', function () {
        Layer.Hide(index)
    }, false)
    del.addEventListener('click', function () {
        Layer.Delete(index)
    }, false)
    pre.addEventListener('click', function () {
        Layer.Preview(index)
    }, false)
}

console.log(`show ${c.width} x ${c.height}`)

var History = {
    history: new Array(new Array),
    redo_list: new Array(new Array),
    
    save: function () {
        this.redo_list[Layer.current] = [];
        this.history[Layer.current].push(c.toDataURL())
        Layer.Preview(Layer.current)
        console.log(`save (${this.history[Layer.current].length})(${Layer.current})`)
    },

    undo: function () {
        if (this.history[Layer.current].length > 1) {
            console.log(`undo (${this.history[Layer.current].length})`)
            this.redo_list[Layer.current].push(this.history[Layer.current].pop())
            this.draw()
        }
    },

    redo: function () {
        if (this.redo_list[Layer.current].length > 0) {
            console.log('redo')
            this.history[Layer.current].push(this.redo_list[Layer.current].pop())
            this.draw()
        }
    },

    draw: function () {
        if (this.history[Layer.current].length > 0) {
            var img = new Image()
            img.onload = function () {
                //ctx.clearRect(0, 0, c.width, c.height)
                ctx.globalCompositeOperation="copy";
                ctx.drawImage(img, 0, 0)
                console.log('draw')
                ctx.globalCompositeOperation="source-over";
            }
            img.src = this.history[Layer.current][this.history[Layer.current].length-1];
        }
    },

    load: function (callback) {
        var img = new Image()
        img.onload = function () {
            //ctx.clearRect(0, 0, c.width, c.height)
            ctx.globalCompositeOperation="copy";
            ctx.drawImage(img, 0, 0)
            console.log('draw')
            ctx.globalCompositeOperation="source-over";
            callback()
        }
        img.src = this.history[Layer.current][this.history[Layer.current].length-1];
    },

    add: function () {
        this.history.push(new Array())
        this.redo_list.push(new Array())
        this.save()
    },

    delete: function () {
        this.history.pop()
        this.redo_list.pop()
    },

    clear: function () {
        this.history = new Array(new Array)
        this.redo_list = new Array(new Array)
    }
}