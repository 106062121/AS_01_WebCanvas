var lastX, lastY;
var globalX, globalY;
var mouse_pressed = false;
var mouse_locked = false;

var global_color = "#FEA0CF";
var global_width = 20;
var global_font = "Arial"
var cursor_offset = 0;
var img = new Image();

//Get elements
brush = document.getElementById('brush')
eraser = document.getElementById('eraser')
text = document.getElementById('text')
reset = document.getElementById('reset')
undo = document.getElementById('undo')
redo = document.getElementById('redo')
upload = document.getElementById('upload')
download = document.getElementById('download')
configure1 = document.getElementById('configure1')
//datalist1 = document.getElementById('datalist1')
configure2 = document.getElementById('configure2')
//datalist2 = document.getElementById('datalist2')
color = document.getElementById('color')
circle = document.getElementById('circle')
rectangle = document.getElementById('rectangle')
triangle = document.getElementById('triangle')
fill = document.getElementById('fill')

var Utilities = {
    brush_enabled: true,
    eraser_enabled: false,
    text_enabled: false,
    img_enabled: false,
    circle_enabled: false,
    rectangle_enabled: false,
    triangle_enabled: false,
    width_list: [4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48, 60, 72],
    font_list: ["Georgia", "Palatino Linotype", "Times New Roman", "Arial", "Comic Sans MS", "Lucida Sans Unicode"],
    cursor_list: ["url(cursor-lite.png), auto", "url(cursor-dark.png), auto", "text", "crosshair", "cell", "pointer"],

    reset: function () {
        this.brush_enabled = false;
        this.eraser_enabled = false;
        this.text_enabled = false;
        this.img_enabled = false;
        this.circle_enabled = false;
        this.rectangle_enabled = false;
        this.triangle_enabled = false;
        brush.style.backgroundColor = ""
        eraser.style.backgroundColor = ""
        text.style.backgroundColor = ""
        reset.style.backgroundColor = ""
        undo.style.backgroundColor = ""
        redo.style.backgroundColor = ""
        upload.style.backgroundColor = ""
        download.style.backgroundColor = ""
        circle.style.backgroundColor = ""
        rectangle.style.backgroundColor = ""
        triangle.style.backgroundColor = ""
        fill.style.backgroundColor = ""
        console.log('reset_done')
    },

    default: function () {
        this.reset()
        this.update('brush')
    },

    update: function (type) {
        this.reset()
        this[type + '_enabled'] = true;
        try {
            document.getElementById(type).style.backgroundColor = "rgb(130, 130, 130)"
        }
        catch (e) {
            console.log(type);
        }
        if (type === 'brush') {
            document.getElementById('canvas-body').style.cursor = this.cursor_list[5];
        }
        else if (type === 'eraser') {
            document.getElementById('canvas-body').style.cursor = this.cursor_list[4];
        }
        else if (type === 'text') {
            document.getElementById('canvas-body').style.cursor = this.cursor_list[2];
        }
        else if (type === 'img') {
            document.getElementById('canvas-body').style.cursor = ""
        }
        else if (type === 'circle') {
            document.getElementById('canvas-body').style.cursor = this.cursor_list[3];
        }
        else if (type === 'rectangle') {
            document.getElementById('canvas-body').style.cursor = this.cursor_list[3];
        }
        else if (type === 'triangle') {
            document.getElementById('canvas-body').style.cursor = this.cursor_list[3];
        }
        else {
            document.getElementById('canvas-body').style.cursor = "";
        }
    }
}

class Config {
    constructor () {
        this.color = "black";
        this.width = 10;
    }
}

class Text_Config extends Config {
    constructor () {
        super();
        this.font = this.width.toString() + "pt Arial";
    }
}

var brush_config = new Config();
var eraser_config = new Config();
var text_config = new Text_Config();

function Init() {
    History.save()
    Utilities.default()
    for (let i = 0; i < Utilities.font_list.length; i++) {
        let wlist = document.createElement('option')
        wlist.value = Utilities.width_list[i];
        wlist.innerHTML = Utilities.width_list[i];
        configure1.appendChild(wlist);

        let flist = document.createElement('option')
        flist.value = Utilities.font_list[i];
        flist.innerHTML = Utilities.font_list[i];
        if (i === 3) {
            flist.selected = "selected";
        }
        configure2.appendChild(flist);
    }

    for (let i = Utilities.font_list.length; i < Utilities.width_list.length; i++) {
        let wlist = document.createElement('option')
        wlist.value = Utilities.width_list[i];
        wlist.innerHTML = Utilities.width_list[i];
        if (i === 8) {
            wlist.selected = "selected";
        }
        configure1.appendChild(wlist);
    }
}

function Draw (x, y) {
    if (Utilities.brush_enabled) {
        ctx.beginPath()
        ctx.strokeStyle = global_color;
        ctx.lineWidth = global_width;
        ctx.lineJoin = "round";
        ctx.moveTo(lastX + cursor_offset, lastY + cursor_offset)
        ctx.lineTo(x + cursor_offset, y + cursor_offset)
        ctx.closePath()
        ctx.stroke()

        lastX = x;
        lastY = y;
    }
    else if (Utilities.eraser_enabled) {
        ctx.beginPath()
        ctx.globalCompositeOperation="destination-out";
        ctx.strokeStyle = "rgba(0,0,0,1)";
        ctx.lineWidth = global_width;
        ctx.lineJoin = "round";
        ctx.moveTo(lastX + cursor_offset, lastY + cursor_offset)
        ctx.lineTo(x + cursor_offset, y + cursor_offset)
        ctx.closePath()
        ctx.stroke()
        ctx.globalCompositeOperation="source-over";

        lastX = x;
        lastY = y;
    }
}

function Draw_Fixed (x, y, str) {
    if (Utilities.text_enabled) {
        ctx.font = global_width + "pt " + global_font;
        ctx.fillStyle = global_color;
        ctx.fillText(str, Number(x.replace("px", "")), Number(y.replace("px", "")))
    }
    else if (Utilities.img_enabled) {
        ctx.drawImage(img, 0, 0, img.width, img.height, x, y, img.width * str / 100, img.height * str / 100)
    }
    else if (Utilities.circle_enabled) {
        ctx.beginPath()
        ctx.fillStyle = global_color;
        ctx.arc(lastX, lastY, Math.sqrt(Math.pow(Math.abs(x - lastX), 2) + Math.pow(Math.abs(y - lastY), 2)), 0, Math.PI * 2, false)
        ctx.fill()
    }
    else if (Utilities.rectangle_enabled) {
        ctx.fillStyle = global_color;
        ctx.fillRect(lastX, lastY, x - lastX, y - lastY)
    }
    else if (Utilities.triangle_enabled) {
        ctx.beginPath()
        ctx.fillStyle = global_color;
        ctx.moveTo(lastX, lastY)
        ctx.lineTo(lastX, y)
        ctx.lineTo(x, y)
        ctx.fill()
    }
}

function Draw_Dynamic (x, y, scale) {
    if (Utilities.img_enabled) {
        if (mouse_locked) {
            ctx.drawImage(img, 0, 0, img.width, img.height, x, y, img.width * scale / 100, img.height * scale / 100)
        }
        else {
            ctx.drawImage(img, x, y)
        }
    }
    else if (Utilities.circle_enabled) {
        ctx.beginPath()
        ctx.fillStyle = global_color;
        ctx.arc(lastX, lastY, Math.sqrt(Math.pow(Math.abs(x - lastX), 2) + Math.pow(Math.abs(y - lastY), 2)), 0, Math.PI * 2, false)
        ctx.fill()
    }
    else if (Utilities.rectangle_enabled) {
        ctx.fillStyle = global_color;
        ctx.fillRect(lastX, lastY, x - lastX, y - lastY)
    }
    else if (Utilities.triangle_enabled) {
        ctx.beginPath()
        ctx.fillStyle = global_color;
        ctx.moveTo(lastX, lastY)
        ctx.lineTo(lastX, y)
        ctx.lineTo(x, y)
        ctx.fill()
    }
}

function Reset () {
    ctx.clearRect(0, 0, c.width, c.height)
    //document.getElementById('canvas-body').style.cursor = "default";
}

function Fill () {
    ctx.fillStyle = global_color;
    ctx.fillRect(0, 0, c.width, c.height)
}

function Cursor_Color_Pick (x, y) {
    var p = ctx.getImageData(x + 16, y + 16, 1, 1).data;
    console.log(p);
}


//Old History
/*var History = {
    history: [],
    redo_list: [],
    last: new Image(),
    
    save: function () {
        this.redo_list = [];
        this.history.push(c.toDataURL())
        console.log(`save (${this.history.length})`)
    },

    undo: function () {
        if (this.history.length > 1) {
            console.log(`undo (${this.history.length})`)
            this.redo_list.push(this.history.pop())
            this.draw()
        }
    },

    redo: function () {
        if (this.redo_list.length > 0) {
            console.log('redo')
            this.history.push(this.redo_list.pop())
            this.draw()
        }
    },

    draw: function () {
        if (this.history.length > 0) {
            var img = new Image()
            img.onload = function () {
                //ctx.clearRect(0, 0, c.width, c.height)
                ctx.globalCompositeOperation="copy";
                ctx.drawImage(img, 0, 0)
                console.log('draw')
                ctx.globalCompositeOperation="source-over";
            }
            img.src = this.history[this.history.length-1];
        }
    },

    load: function (callback) {
        var img = new Image()
        img.onload = function () {
            //ctx.clearRect(0, 0, c.width, c.height)
            ctx.globalCompositeOperation="copy";
            ctx.drawImage(img, 0, 0)
            console.log('draw')
            ctx.globalCompositeOperation="source-over";
            callback()
        }
        img.src = this.history[this.history.length-1];
    }
}
*/